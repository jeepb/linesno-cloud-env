# Jenkins权限插件配置

## 本内容你将获得
- Jenkins安装及插件配置
- Jenkins权限配置

## 插件安装
### 说明
jenkins本身带有一定的权限配置,但是通用情况下，更推荐一个细分的权限插件`Role-based Authorization Strategy`

### 插件安装
> 此处参考上面的jenkins安装，有插件安装教程

- 略

### 权限配置
在插件安装完成之后，还不能出现插件的配置，需要调整插件配置策略,选择`Role-Based Strategy`
<p align="center"><img src="/images/jenkins_15.jpg" width="800px" /></p>

在选择之后，返回jenkins配置，出现以下的选择功能，点击进去配置即可
<p align="center"><img src="/images/jenkins_16.jpg" width="800px" /></p>

配置目标是建立一个开发角色,点击进入`Manager Roles`配置,功能选择按如下即可
<p align="center"><img src="/images/jenkins_16.jpg" width="800px" /></p>



