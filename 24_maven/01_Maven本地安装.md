# Maven配置

## 本章节你将获取
- maven配置私服地址
- maven插件账号配置

## 配置
### 添加本地环境变量
解压maven
```bash
tar -zxvf apache-maven-3.6.0-bin.tar.gz
```

配置本地环境变量
```bash
vim ~/.bash_profile
```

添加maven环境变量
```bash
# maven env
# 此按本地实际目录配置
export M2_HOME=~/Downloads/apache-maven-3.6.0
export M2=$M2_HOME/bin
# export MAVEN_OPTS="-Xmx1048m -Xms256m -XX:MaxPermSize=312M"
export PATH=$M2:$PATH
```

使环境变量生效
```bash
source ~/.bash_profile
mvn -v # 查看mvn命令
```

### 配置私服
修改`conf/settings.xml`文件
```bash
cd apache-maven-3.6.0
vim conf/settings.xml
```

替换内容为:
```xml

<?xml version="1.0" encoding="UTF-8"?>
<settings xmlns="http://maven.apache.org/SETTINGS/1.0.0"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.0.0 http://maven.apache.org/xsd/settings-1.0.0.xsd">

	<!-- 本地地址 <localRepository>d://m2/repository</localRepository> -->

	<!--配置权限,使用默认用户 -->
	<servers>
		<server>
			<id>releases</id>
			<username>deployment</username>
			<password>1234qwer</password>
		</server>
		<server>
			<id>snapshots</id>
			<username>deployment</username>
			<password>1234qwer</password>
		</server>
	</servers>

	<mirrors>
		<mirror>
			<id>linesno-nexus</id>
			<mirrorOf>*</mirrorOf>
			<name>linesno nexus</name>
			<url>http://192.168.1.110:8081/nexus/content/groups/public</url>
		</mirror>
	</mirrors>

	<profiles>
		<profile>
			<id>linesno</id>
			<activation>
				<activeByDefault>false</activeByDefault>
				<jdk>1.8</jdk>
			</activation>
			<repositories>
				<!-- 私有库地址 -->
				<repository>
					<id>nexus</id>
					<url>http://192.168.1.110:8081/nexus/content/groups/public/</url>
					<releases>
						<enabled>true</enabled>
					</releases>
					<snapshots>
						<enabled>true</enabled>
					</snapshots>
				</repository>
			</repositories>
			<pluginRepositories>
				<!--插件库地址 -->
				<pluginRepository>
					<id>nexus</id>
					<url>http://192.168.1.110:8081/nexus/content/groups/public/</url>
					<releases>
						<enabled>true</enabled>
					</releases>
					<snapshots>
						<enabled>true</enabled>
					</snapshots>
				</pluginRepository>
			</pluginRepositories>
		</profile>

		<profile>
			<id>sonarqube</id>
			<activation>
				<activeByDefault>false</activeByDefault>
				<jdk>1.8</jdk>
			</activation>
			<properties>
				<sonar.host.url>http://sonarqube.linesno.com</sonar.host.url>
			</properties>
		</profile>

	</profiles>

	<!--激活profile -->
	<activeProfiles>
		<activeProfile>linesno</activeProfile>
		<activeProfile>sonarqube</activeProfile>
	</activeProfiles>

</settings>

```
### 测试
初始化Maven
```bash
mvn help:system
```

本地构建打包,以工程`https://gitee.com/landonniao/linesno-cloud-guide.git`为例子

> pom.xml文件中的nexus.repository属性修改成私服地址的IP,否则无法上传,关于例子，然后建立服务的时候，再具体说明。

```bash
git clone https://gitee.com/landonniao/linesno-cloud-guide.git
cd linesno-cloud-guide
# 测试maven命令(打包/本地安装/发布到私服)
mvn clean package install deploy
```

私服上查看包
<p align="center"><img src="/images/nexus_07.png" width="800px" /></p>

此配置成功

### 配置aliyun镜像上传
阿里云镜像仓库地址
```
https://cr.console.aliyun.com
```

创建命名空间`alinesno-cloud`,如果没有创建空间则上传的时候出异常

配置maven,在settings.xml添加阿里云认证，在`servers`标签里面添加
```xml
<server>
    <id>docker-aliyun</id>
    <username>阿里云账号</username>
    <password>仓库密码(非阿里云密码)</password>
    <configuration>
        <email>阿里云账号</email>
    </configuration>
</server>
```

在parent父类的`pom.xml`里面添加docker打包插件
```xml

<!-- docker registry 忏悔 -->
<docker.repostory>registry.cn-shenzhen.aliyuncs.com</docker.repostory>
<docker.registry.name>alinesno-cloud</docker.registry.name>
<docker.maven.plugin.version>0.4.13</docker.maven.plugin.version>

<!-- docker 插件 -->
<plugin>
    <groupId>com.spotify</groupId>
    <artifactId>docker-maven-plugin</artifactId>
    <version>${docker.maven.plugin.version}</version>
    <configuration>
        <!-- 镜像中心 -->
        <serverId>docker-aliyun</serverId>
        <registryUrl>${docker.repostory}</registryUrl>

        <!-- 优化参数 -->
        <forceTags>true</forceTags>
        <imageTags>
            <imageTag>${project.version}</imageTag>
            <imageTag>latest</imageTag>
        </imageTags>

        <!-- 打包 -->
        <imageName>${docker.repostory}/${docker.registry.name}/${project.artifactId}:${project.version}</imageName>
        <baseImage>java:8</baseImage>
        <entryPoint>["java", "-Djava.security.egd=file:/dev/./urandom", "-jar", "/ROOT/${project.build.finalName}.jar"]</entryPoint>
        <resources>
            <resource>
                <targetPath>/ROOT/</targetPath>
                <directory>${project.build.directory}</directory>
                <include>${project.build.finalName}.jar</include>
            </resource>
        </resources>
    </configuration>
</plugin>
```

`插件里面的serverId要与maven配置的server的id要一样`

在sts中，打包镜像并上传命令
```bash
mvn clean package docker:build docker:push
```
<p align="center"><img src="/images/maven_01.jpg" width="800px" /></p>

`上传速度看网络,建立在命令行中配置`

## Ansible构建

## 镜像
- 构建镜像
- 使用

## 参考资料
- [GitBook官网](http://www.baidu.com)
